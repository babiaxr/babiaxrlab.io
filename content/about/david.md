---
name: "David Moreno Lumbreras"
weight: 10
draft: false
avatar : "images/0.jpeg"
github : "dlumbrer"
gitlab : "dlumbrer"
linkedin: "david-moreno-lumbreras-597ab5101"
twitter: "dlumbrer1"
mail: "dmorenolumb@gmail.com"
---
Front-end Developer at Bitergia and PhD student at University Rey Juan Carlos